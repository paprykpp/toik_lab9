package com.demo.springboot;

import com.demo.springboot.dto.MovieDto;
import com.demo.springboot.dto.MovieListDto;
import java.util.*;
import java.util.stream.Collectors;

public class MovieApi {
    private final ArrayList<MovieDto> movieList;
    private int currentId = 0;

    public MovieApi() {
        this.movieList = new ArrayList<>();
    }

    public List<MovieDto> getMovies() {
        movieList.sort(Comparator.comparingInt(MovieDto::getMovieId).reversed());
        return movieList;
    }

    public void addMovie(MovieListDto movieListDto) {
        movieList.add(new MovieDto(currentId++,
                movieListDto.getTitle(),
                movieListDto.getYear(),
                movieListDto.getImage()));
    }

    public void updateMovie(Integer id, MovieListDto movieListDto){
        movieList.stream()
                .filter(item -> item.getMovieId().equals(id))
                .peek(item -> {
                    item.setTitle(movieListDto.getTitle());
                    item.setYear(movieListDto.getYear());
                    item.setImage(movieListDto.getImage());
                })
                .findAny()
                .orElseThrow();
    }

    public void deleteMovie(Integer id) {
        currentId = id;
        movieList.remove(
                movieList.stream()
                        .filter(item -> item.getMovieId().equals(id))
                        .findAny()
                        .orElseThrow()
        );
        movieList.stream()
                .filter(item -> item.getMovieId() > id)
                .peek(item -> {
                    item.setMovieId(item.getMovieId() - 1);
                    currentId++;
                })
                .filter(item -> item.getMovieId() < id)
                .peek(item -> {
                    item.setMovieId(item.getMovieId() + 1);
                    currentId--;
                })
                .collect(Collectors.toList());
    }
}
