package com.demo.springboot.rest;

import com.demo.springboot.dto.MovieDto;
import com.demo.springboot.dto.MovieListDto;
import com.demo.springboot.MovieApi;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.List;

@RestController
@RequestMapping(value = "/api")
public class MovieApiController {
    MovieApi movieApi = new MovieApi();

    public MovieApiController() {

    }

    @GetMapping("/movies")
    public ResponseEntity<List<MovieDto>> getMovies() {
        return ResponseEntity.status(200).body(movieApi.getMovies());
    }

    @PostMapping("/movies")
    public ResponseEntity<Void> createMovie(@RequestBody MovieListDto movieListDto){
        try {
            if (movieListDto.getTitle() == null || movieListDto.getYear() == null || movieListDto.getImage() == null) {
                throw new Exception();
            }
            movieApi.addMovie(movieListDto);
        }catch (Exception e){
            return ResponseEntity.status(400).build();
        }
        return ResponseEntity.status(201).build();
    }

    @PutMapping("/movies/{id}")
    public ResponseEntity<Void> updateMovie(@PathVariable ("id") Integer id, @RequestBody MovieListDto movieListDto){
        try {
            movieApi.updateMovie(id, movieListDto);
        }catch (Exception e){
            return ResponseEntity.status(404).build();
        }
        return ResponseEntity.status(200).build();
    }

    @DeleteMapping("/movies/{id}")
    public ResponseEntity<Void> deleteMovie(@PathVariable ("id")Integer id){
        try {
            movieApi.deleteMovie(id);
        }catch (Exception e) {
            return ResponseEntity.status(404).build();
        }
        return ResponseEntity.status(200).build();
    }
}
